package com.frooooonk.foobar

object Foobar {
    const val bar = "bar"

    fun foo() = bar
}