package com.frooooonk.foobar

import org.junit.Assert.assertEquals
import org.junit.Test


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class FoobarTest {
    @Test
    fun foo_returns_bar() {
        assertEquals(Foobar.bar, Foobar.foo())
    }
}